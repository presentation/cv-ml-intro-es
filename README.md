## Resumen

Qué son "Visión por computador" y "Aprendizaje de máquina"? 

Una presentación sobre los avances de la inteligencia artificial desde el punto de vista de las aplicaciones actuales de VC y AM. La IA como la electricidad del futuro, la próxima revolución.